package provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.HashMap;

/**
 * Created by bouluad on 07/02/17.
 */
public class ClientsProvider extends ContentProvider {

    public static final String PROVIDER_NAME = "clientsprovider";
    public static final String URL = "content://" + PROVIDER_NAME ;
    public static final Uri CONTENT_URI = Uri.parse(URL);

    public static final String _ID = "id";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String BIRTHDATE = "birthdate";
    public static final String CITY = "city";
    static final int CLIENTS = 1;
    static final int CLIENT_ID = 2;
    static final UriMatcher uriMatcher;
    static final String DATABASE_NAME = "Client";
    static final String CLIENTS_TABLE_NAME = "clients";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DB_TABLE =
            "CREATE TABLE if not exists " + CLIENTS_TABLE_NAME + " (" +
                    _ID + " integer PRIMARY KEY autoincrement," +
                    FIRSTNAME + "," +
                    LASTNAME + "," +
                    BIRTHDATE + "," +
                    CITY +
                    " );";
    private static HashMap<String, String> CLIENTS_PROJECTION_MAP;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "CLIENTS", CLIENTS);
        uriMatcher.addURI(PROVIDER_NAME, "CLIENTS/#", CLIENT_ID);
    }

    /**
     * Database specific constant declarations
     */

    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        Context context = getContext();
        DatabaseHelper dbHelper = new DatabaseHelper(context);

        /**
         * Create a write able database which will trigger its
         * creation if it doesn't already exist.
         */

        db = dbHelper.getWritableDatabase();
        return (db == null) ? false : true;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        /**
         * Add a new student record
         */
        long rowID = db.insert(CLIENTS_TABLE_NAME, "", values);

        /**
         * If record is added successfully
         */
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public Cursor query(Uri uri, String[] projection,
                        String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(CLIENTS_TABLE_NAME);

        switch (uriMatcher.match(uri)) {
            case CLIENTS:
                qb.setProjectionMap(CLIENTS_PROJECTION_MAP);
                break;

            case CLIENT_ID:
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;

            default:
        }

        if (sortOrder == null || sortOrder == "") {
            /**
             * By default sort on student names
             */
            sortOrder = FIRSTNAME;
        }

        Cursor c = qb.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        /**
         * register to watch a content URI for changes
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case CLIENTS:
                count = db.delete(CLIENTS_TABLE_NAME, selection, selectionArgs);
                break;

            case CLIENT_ID:
                String id = uri.getPathSegments().get(1);

                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values,
                      String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case CLIENTS:
                count = db.update(CLIENTS_TABLE_NAME, values, selection, selectionArgs);
                break;

            case CLIENT_ID:

                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            /**
             * Get all student records
             */
            case CLIENTS:
                return "vnd.android.cursor.dir/vnd.provider.clients";
            /**
             * Get a particular student
             */
            case CLIENT_ID:
                return "vnd.android.cursor.item/vnd.provider.clients";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    /**
     * Helper class that actually creates and manages
     * the provider's underlying data repository.
     */

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + CLIENTS_TABLE_NAME);
            onCreate(db);
        }
    }
}