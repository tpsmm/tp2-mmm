package istic.fr.tp2;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;

import provider.ClientsProvider;

/**
 * Created by bouluad on 07/02/17.
 */
public class AddActivity extends AppCompatActivity {

    int d;
    int m;
    int y;
    EditText nom;
    EditText prenom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addclient);

        nom = (EditText) findViewById(R.id.nomedittxt);
        prenom = (EditText) findViewById(R.id.prenomedittxt);
        final CalendarView datenaissance = (CalendarView) findViewById(R.id.simpleCalendarView);

        datenaissance.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {

                d = dayOfMonth;
                m = month;
                y = year;
            }
        });


        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        Button valider = (Button) findViewById(R.id.validebtn);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ContentValues contact = new ContentValues();
                contact.put("firstname", nom.getText().toString());
                contact.put("lastname", prenom.getText().toString());
                contact.put("birthdate", d + "/" + (m + 1) + "/" + y);
                contact.put("city", spinner.getSelectedItem().toString());

                Uri uri = getContentResolver().insert(
                        ClientsProvider.CONTENT_URI, contact);


                Intent myIntent = new Intent(AddActivity.this, MainActivity.class);
                AddActivity.this.startActivity(myIntent);
            }
        });
    }


}
