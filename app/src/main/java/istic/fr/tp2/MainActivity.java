package istic.fr.tp2;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import provider.ClientsProvider;

public class MainActivity extends AppCompatActivity {


    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);
        Button addClientBtn = (Button) findViewById(R.id.addClient);

        addClientBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(MainActivity.this, AddActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });


        //Création de la ArrayList qui nous permettra de remplire la listView
        ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();

        //On déclare la HashMap qui contiendra les informations pour un item
        HashMap<String, String> map;

        map = new HashMap<String, String>();

        // Retrieve student records
        String URL = "content://clientsprovider";

        Uri clients = Uri.parse(URL);
        Cursor c = managedQuery(clients, null, null, null, "firstname");

        if (c.moveToFirst()) {
            do {

                map.put("firstname", c.getString(c.getColumnIndex(ClientsProvider.FIRSTNAME)));
                map.put("lastname", c.getString(c.getColumnIndex(ClientsProvider.LASTNAME)));
                map.put("birthdate", c.getString(c.getColumnIndex(ClientsProvider.BIRTHDATE)));
                map.put("city", c.getString(c.getColumnIndex(ClientsProvider.CITY)));
                listItem.add(map);

            } while (c.moveToNext());
        }

        //Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (listItem) dans la vue affichageitem
        SimpleAdapter mSchedule = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                new String[]{"firstname", "lastname", "birthdate"}, new int[]{R.id.firstname, R.id.lastname, R.id.birthdate});

        //On attribut à notre listView l'adapter que l'on vient de créer
        listView.setAdapter(mSchedule);

        //On met un écouteur d'évènement sur notre listView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                HashMap<String, String> map = (HashMap<String, String>) listView.getItemAtPosition(position);
                AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                adb.setTitle("Client : ");
                adb.setMessage("La ville : " + map.get("city"));
                adb.setPositiveButton("Ok", null);
                adb.show();
            }
        });

    }


    private void insertRecords() {
        ContentValues contact = new ContentValues();
        contact.put("firstname", "Mohammed");
        contact.put("lastname", "BOULUAD");
        contact.put("birthdate", "07/08/1993");
        contact.put("city", "Nador");

        Uri uri = getContentResolver().insert(
                ClientsProvider.CONTENT_URI, contact);

    }
}
